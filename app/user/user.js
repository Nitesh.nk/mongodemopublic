const userService = {};
const bcrypt = require("bcrypt");
const logger = require('../../core/common/logger');
const customError = require('../../core/common/errorHander');
const userModel = require('./index');
const schemavalidate = require('./validationSchema');
const authManager = require("../auth/auth");
const mongoose = require('mongoose');
const uploadtoserver = require('../../core/common/upload')
const Constants = require('../../Constants');

userService.login = async function (reqObj, res) {
    try {
        const { mobileEmail, password } = reqObj.body;
        let checkmsg = null;
        await schemavalidate.login.validate(reqObj.body).catch(async (err) => {
            checkmsg = err.message;
        });
        if (checkmsg) {
            const errmsg = `In user.login error occured for data ${mobileEmail} while yup validation ${checkmsg}`;
            let error = customError.customError(Constants.statusvalidation_code, new Error(checkmsg), errmsg, null);
            return res.json(error);
        } else {
            const userData = await userModel.findOne({
                $and: [{ $or: [{ email: mobileEmail }, { mobile: mobileEmail }] },
                { is_active: Constants.active }]
            })
                .select({ "otp": 0, "__v": 0, "file_path": 0 })
                .lean()

            if (userData) {
                const correctPass = await bcrypt.compareSync(password || "b", userData.password || "a"); // bcrypt Comparing password with hashpwd
                if (correctPass) {
                    // return true if matched or else return false
                    delete userData.password; // Not required fields
                    const token = await authManager.generateToken(userData, {});
                    userData.token = token;
                    return res.json({ status: Constants.statusOK_code, message: Constants.statusOK_msg, data: userData });

                } else {
                    const errmsg = `In user.login ${mobileEmail}  was invalid user(Password not matched)`;
                    let error = customError.customError(Constants.statusvalidation_code, new Error(Constants.Incorrectpassword_msg), errmsg, null);
                    return res.json(error);
                }
            } else {
                const errmsg = `In user.login  ${mobileEmail} was invalid user(User does not exist) `;
                let error = customError.customError(Constants.statusinvaliduser_code, new Error(Constants.Invalid_user_msg), errmsg, null);
                return res.json(error);
            }
        }
    } catch (err) {
        const errmsg = `Some error occured in user.login for data ${reqObj.body.mobileEmail} `
        let error = customError.customError(Constants.statusissue_code, new Error(Constants.issue_msg), errmsg, err);
        return res.json(error);
    }
}

userService.createUser = async function (reqObj, res) {
    try {
        const { first_name, last_name, email, mobile, password, roleType } = reqObj.body;
        let checkmsg = null;
        await schemavalidate.createUser.validate(reqObj.body).catch(async (err) => {
            checkmsg = err.message;
        });
        if (checkmsg) {
            const errmsg = `In user.createUser error occured while yup validation ${checkmsg}`;
            let error = customError.customError(Constants.statusvalidation_code, new Error(checkmsg), errmsg, null);
            return res.json(error);
        } else {
            let hash = bcrypt.hashSync(password, Constants.saltRounds);
            let roleId = roleType == 1 ? new mongoose.Types.ObjectId(Constants.roleIdUser) : new mongoose.Types.ObjectId(Constants.roleIdDoctor);
            const userExistCheck = await userModel.findOne({
                $and: [{ $or: [{ email }, { mobile }] },
                { is_active: Constants.active }, { role: roleId }]
            })
                .select("-__v").select({ "otp": 0 })
                .lean()
            if (userExistCheck) {
                const errmsg = `In user.createUser user already exist with data ${JSON.stringify(reqObj.body)} `
                let error = customError.customError(Constants.statusissue_code, new Error(`User ${Constants.alreadyExist} with this email/mobile and role`), errmsg, null);
                return res.json(error);
            }
            else {
                let userData = new userModel({
                    role: roleId,
                    first_name,
                    last_name,
                    email,
                    mobile,
                    password: hash,
                    is_active: Constants.active,
                    created_by: "createUser"
                });
                await userData.save()
                    .then(() => {
                        return res.json({ status: Constants.statusOK_code, message: Constants.statusOK_msg });
                    })
            }
        }
    } catch (err) {
        const errmsg = `Some error occured in user.createUser for data ${JSON.stringify(reqObj.body)} `
        let error = customError.customError(Constants.statusissue_code, new Error(Constants.issue_msg), errmsg, err);
        return res.json(error);
    }
}

userService.userList = async function (reqObj, res) {
    try {
        const { roleId } = reqObj;
        const { firstName, lastName, email, mobile, } = reqObj.body;
        let filterCondition = [{ is_active: Constants.active },
        { role: roleId == Constants.roleIdUser ? new mongoose.Types.ObjectId(Constants.roleIdDoctor) : new mongoose.Types.ObjectId(Constants.roleIdUser) }];

        if (firstName)
            filterCondition.push({ first_name: { $regex: `^${firstName}`, $options: 'i' } })
        if (lastName)
            filterCondition.push({ last_name: { $regex: `^${lastName}`, $options: 'i' } })
        if (email)
            filterCondition.push({ email: { $regex: `^${email}`, $options: 'i' } })
        if (mobile)
            filterCondition.push({ mobile: { $regex: `^${mobile}`, $options: 'i' } })
        const userData = await userModel.find({
            $and: filterCondition
        })
            .select({ "otp": 0, "__v": 0, "role": 0, "password": 0 })
            .lean()
        return res.json({ status: Constants.statusOK_code, message: Constants.statusOK_msg, data: userData });


    } catch (err) {
        const errmsg = `Some error occured in user.userList for data ${reqObj.body.mobileEmail} `
        let error = customError.customError(Constants.statusissue_code, new Error(Constants.issue_msg), errmsg, err);
        return res.json(error);
    }
}

userService.uploadDoc = async function (reqObj, res) {
    try {
        const { userId } = reqObj;
        let errorFile = [];
        let successFile = [];
        if (reqObj.files && reqObj.files.file) {
            let fileObj = [];
            if (reqObj.files.file.name) {
                fileObj = [reqObj.files.file];
            } else {
                fileObj = reqObj.files.file;
            }
            if (fileObj.length > 5) {
                const errmsg = `In user.uploadDoc user ${userId} exceed file upload limit of 5`;
                let error = customError.customError(Constants.statusvalidation_code, new Error(Constants.maxFile), errmsg, null);
                return res.json(error);
            }

            for (let i = 0; i < fileObj.length; i++) {
                if (fileObj[i].size > 10485760) {
                    errorFile.push(fileObj[i].name + " size is greater then 10mb")
                } else {
                    let uploadStatus = await uploadtoserver.uploadtoserver(fileObj[i]);
                    if (uploadStatus.status)
                        successFile.push(uploadStatus.filename)
                    else
                        errorFile.push(uploadStatus.message)
                }
            }
            // if (reqObj.files.file.name.split(".")[1] == "xls")
            if (!successFile.length) {
                // const errmsg = `In user.uploadDoc file not uploaded by user ${userId} for following file ${errorFile.toString()}`;
                // let error = customError.customError(Constants.statusfail_code, new Error("File is required"), errmsg, null);
                // return res.json(error);
                return res.json({ status: Constants.statusOK_code, message: Constants.statusOK_msg, errorFile: errorFile });

            } else {
                await userModel.findOneAndUpdate(
                    { $and: [{ is_active: Constants.active }, { _id: new mongoose.Types.ObjectId(userId) }] },
                    {
                        $set: {
                            file_path: successFile,
                            updated_by: reqObj.userId,
                        }
                    })
                return res.json({ status: Constants.statusOK_code, message: Constants.statusOK_msg, errorFile: errorFile });
            }
        } else {
            const errmsg = `In user.uploadDoc files not provided by user ${userId}`;
            let error = customError.customError(Constants.statusvalidation_code, new Error("File is required"), errmsg, null);
            return res.json(error);
        }
        return res.json({ status: Constants.statusOK_code, message: Constants.statusOK_msg, data: userData });


    } catch (err) {
        const errmsg = `Some error occured in user.uploadDoc for data ${reqObj.body.mobileEmail} `
        let error = customError.customError(Constants.statusissue_code, new Error(Constants.issue_msg), errmsg, err);
        return res.json(error);
    }
}
module.exports = userService;
