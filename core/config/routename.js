let allroutes = {};

allroutes.routeNames = ['user'];
allroutes.userRoute = ['login', 'createUser'];
allroutes.authUserRoute = ['userList', 'uploadDoc']

module.exports = allroutes;
