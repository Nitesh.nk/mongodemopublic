/* eslint-disable prefer-promise-reject-errors */
/* eslint-disable import/no-extraneous-dependencies */
const uploadservice = {};
const AWS = require('aws-sdk');
const Constants = require('../../Constants');
const winston = require('../common/logger');

const s3Client = new AWS.S3({
  accessKeyId: Constants.accessKeyId,
  secretAccessKey: Constants.secretAccessKey,
  region: Constants.region,
});

uploadservice.upload = async function uploadfile(Key, data) {
  return new Promise(async (resolve, reject) => {
    const uploadParams = {
      Bucket: Constants.Bucket,
      Key, // pass key
      Body: data
    };

    await s3Client.putObject(uploadParams, (err, s3data) => {
      if (err) {
        reject({ Errro: err });
      } else {
        console.log(s3data);
        resolve(s3data.Location);
      }
    });
  });
};

uploadservice.uploadbase64 = async function uploadfile(Key, data, imagetype) {
  return new Promise(async (resolve, reject) => {
    const uploadParams = {
      ContentEncoding: 'base64',
      Bucket: Constants.Bucket
    };
    const params = uploadParams;

    params.Key = Key;
    params.Body = data;
    params.ContentType = `image/${imagetype}`

    await s3Client.putObject(params, (err, s3data) => {
      if (err) {
        reject({ Errro: err });
      } else {
        console.log(s3data);
        resolve(s3data.Location);
      }
    });
  });
};

uploadservice.uploadtoserver = async (filedata) => {
  try {
    let sampleFile = filedata;
    const name = `${Date.now().toString()}_${sampleFile.name}`;
    await sampleFile.mv('./public/' + name)
    return {
      status: true,
      filename: `http://localhost:3016/` + name,
      message: Constants.statusOK_msg
    }
  } catch (err) {
    winston.error("Error while uploading file in upload.js", err);
    return {
      status: false,
      filename: name,
      message: `${Constants.uploadError} ${sampleFile.name}`,
    };
  }
}

module.exports = uploadservice;
